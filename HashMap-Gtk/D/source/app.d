import gtk.Main;
import gtk.TextBuffer;
import gtk.TextView;
import gtk.MainWindow;
import gtk.Label;
import gtk.Box;
import gdk.Event;

import std.stdio;
import std.array;
import std.conv;

class HelloWorld : MainWindow {
    private TextView textView2;
    private TextView textView1;

    this() {
        auto box = new Box (Orientation.HORIZONTAL, 5);
        box.setHomogeneous(true);
        textView1 = new TextView ();
        textView2 = new TextView ();

        super("Map Example");
        setBorderWidth (20);
        setDefaultSize(400, 400);
        textView1.getBuffer.addOnChanged(&buffer1_changed);
        add (box);
        box.add(textView1);
        box.add(textView2);


        showAll ();
    }

    private void buffer1_changed (TextBuffer buf) {
        int[string] map;
        
        foreach (line; buf.getText().split ("\n")) {
            foreach (word ; line.split (" ")) {
                if (word != "") map[word]++;
            }
        }
        string temp = "";
        foreach (key, value; map) {
            temp ~= ((key).to!string ~ " => " ~ (value).to!string ~ "\n");
        }
        textView2.getBuffer.setText(temp);
    }
}

void main (string[] args) {
    Main.init (args);
    new HelloWorld ();
    Main.run ();
}
