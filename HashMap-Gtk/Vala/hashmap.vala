using Gtk;
using Gee;

void buffer1_changed (TextBuffer buf){
	var map = new HashMap<string,int>();
	foreach (var line in buf.text.split("\n")) {
		foreach (var word in line.split(" ")) {
			if(word != "") map[word] = map[word] + 1;
		}
	}
	string temp = "";
	foreach (var item in map.entries) {
		temp += @"$(item.key) => $(item.value)\n";
	}
	textView2.buffer.text = temp;

}

TextView textView2;
public static int main (string[] args) {
	Gtk.init(ref args);

	var window = new Window();
	window.title = "Hello, World!";
	window.border_width = 10;
	window.window_position = WindowPosition.CENTER;
	window.set_default_size(350, 70);
	window.destroy.connect(Gtk.main_quit);
	var box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5);
	var textView1 = new TextView();
	textView2 = new TextView();

	textView1.buffer.changed.connect(buffer1_changed);

	box.homogeneous = true;
	box.add(textView1);
	box.add(textView2);
	window.add(box);
	window.show_all();

	Gtk.main();
	return 0;
}

